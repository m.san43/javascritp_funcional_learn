function Spy(target, method) {
  var obj = target[method]
  var result = {
    count: 0
  }
  target[method] = function() {
    result.count++ 
    return obj.apply(this, arguments)
  }

  return result
}

module.exports = Spy