var fs = require('fs')
var path = require('path')

module.exports = function (ruta, extencion, callback) {

  fs.readdir(ruta, function (err, list) {
    if (err)
      return callback(err);

    list = list.filter(function (file) {
      return path.extname(file) === '.' + extencion
    })

    callback(null, list);
  })
}