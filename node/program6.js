var filterFn = require('./program6_module.js')
var ruta = process.argv[2]
var extencion = process.argv[3]

filterFn(ruta, extencion, function (err, list) {
  if (err)
    return console.error(err)

  list.forEach(function (file) {
    console.log(file)
  })
})